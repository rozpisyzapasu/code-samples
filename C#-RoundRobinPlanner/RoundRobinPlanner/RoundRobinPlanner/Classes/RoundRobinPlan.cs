﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoundRobinPlanner
{
    class RoundRobinPlan
    {
        public int MatchesCount { get; set; }
        public int RoundsCount { get; set; }

        public String ScheduleTime { get; set; }
        public String DateStart { get; set; }
        public String DateEnd { get; set; }

        public bool isEmpty()
        {
            return MatchesCount == 0;
        }
    }
}
