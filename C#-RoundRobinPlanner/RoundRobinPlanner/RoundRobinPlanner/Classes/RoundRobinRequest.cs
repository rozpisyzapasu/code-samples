﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoundRobinPlanner
{
    class RoundRobinRequest
    {
        public int TeamsCount { get; set; }
        public int PeriodsCount { get; set; }

        public int FieldsCount { get; set; }
        public bool AreFieldsParallel { get; set; }

        public String DateStart { get; set; }
        public int MatchDuration { get; set; }
        public int RoundDuration { get; set; }
        public int SpareRoundsCount { get; set; }
    }
}
