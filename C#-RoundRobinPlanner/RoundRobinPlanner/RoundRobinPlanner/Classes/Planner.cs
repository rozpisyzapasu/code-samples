﻿using System;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;

// http://stackoverflow.com/questions/15091300/posting-json-to-url-via-webclient-in-c-sharp
namespace RoundRobinPlanner
{
    class Planner
    {
        private String apiUrl;
        private RoundRobinPlan emptyPlan;

        public Planner(String apiUrl) {
            this.apiUrl = apiUrl;
            emptyPlan = new RoundRobinPlan();
        }

        public RoundRobinPlan calculate(RoundRobinRequest request)
        {            
            try
            {
                string jsonRequest = requestToJson(request);
                var http = createHttpRequest(jsonRequest);
                var jsonResponse = readJsonResponse(http);
                return jsonToPlan(jsonResponse);
            }
            catch (Exception)
            {
                return emptyPlan;
            }
        }

        private static string requestToJson(RoundRobinRequest request)
        {
            var r = new
            {
                scheduleType = "roundRobin/Kirkman",
                teamsCount = request.TeamsCount,
                periodsCount = request.PeriodsCount,
                fields = new
                {
                    count = request.FieldsCount,
                    areFieldsParallel = request.AreFieldsParallel
                },
                durationSettings = new
                {
                    dateStart = request.DateStart,
                    match = new
                    {
                        durationInMinutes = request.MatchDuration
                    },
                    round = new
                    {
                        durationInDays = request.RoundDuration,
                        spareRoundsCount = request.SpareRoundsCount
                    }
                }
            };
            return JsonConvert.SerializeObject(r);
        }

        private HttpWebRequest createHttpRequest(string jsonRequest)
        {
            var http = (HttpWebRequest)WebRequest.Create(new Uri(apiUrl));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";

            string parsedContent = jsonRequest;
            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();
            return http;
        }

        private static string readJsonResponse(HttpWebRequest http)
        {
            var response = http.GetResponse();
            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            return sr.ReadToEnd();
        }

        private static RoundRobinPlan jsonToPlan(string jsonResponse)
        {
            var definition = new
            {
                matchesCount = 0,
                roundsCount = 0,
                durationPlan = new
                {
                    scheduleTime = "",
                    dateStart = "",
                    dateEnd = "",
                }
            };

            var res = JsonConvert.DeserializeAnonymousType(jsonResponse, definition);

            RoundRobinPlan plan = new RoundRobinPlan();
            plan.MatchesCount = res.matchesCount;
            plan.RoundsCount = res.roundsCount;
            if (res.durationPlan != null)
            {
                plan.ScheduleTime = res.durationPlan.scheduleTime;
                plan.DateStart = res.durationPlan.dateStart;
                plan.DateEnd = res.durationPlan.dateEnd;
            }
            return plan;
        }
    }
}
