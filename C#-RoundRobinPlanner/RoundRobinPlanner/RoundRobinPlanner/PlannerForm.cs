﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RoundRobinPlanner;

namespace RoundRobinPlanner
{
    public partial class PlannerForm : Form
    {
        private String apiUrl = "http://www.rozpisyzapasu.cz/api/plan";
        private Planner planner;

        public PlannerForm()
        {
            InitializeComponent();
            textBoxUrl.Text = apiUrl;
            planner = new Planner(apiUrl);
        }

        private void buttonPlan_Click(object sender, EventArgs e)
        {
            RoundRobinRequest request = createRequest();
            RoundRobinPlan plan = calculatePlan(request);
            showPlan(plan);
        }

        private RoundRobinRequest createRequest()
        {
            RoundRobinRequest r = new RoundRobinRequest();
            r.TeamsCount = (int)numericUpDownTeams.Value;
            r.PeriodsCount = (int)numericUpDownPeriods.Value;
            r.FieldsCount = (int)numericUpDownFields.Value;
            r.AreFieldsParallel = checkBoxParallelFields.Checked;
            r.DateStart = textBoxDateStart.Text;
            r.MatchDuration = (int)numericUpDownMatch.Value;
            r.RoundDuration = (int)numericUpDownRound.Value;
            r.SpareRoundsCount = (int)numericUpDownSpare.Value;
            return r;
        }

        private RoundRobinPlan calculatePlan(RoundRobinRequest request)
        {
            return planner.calculate(request);
        }

        private void showPlan(RoundRobinPlan plan)
        {
            labelMatchesCount.Text = plan.isEmpty() ? "?" : plan.MatchesCount.ToString();
            labelRoundCount.Text = plan.isEmpty() ? "?" : plan.RoundsCount.ToString();
            labelDateStart.Text = plan.DateStart == String.Empty ? "?" : plan.DateStart;
            labelDateEnd.Text = plan.DateEnd == String.Empty ? "?" : plan.DateEnd;
            labelDuration.Text = plan.ScheduleTime == String.Empty ? "?" : plan.ScheduleTime;
        }
    }
}
