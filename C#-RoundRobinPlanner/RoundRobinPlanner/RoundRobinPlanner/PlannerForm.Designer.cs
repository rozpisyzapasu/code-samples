﻿namespace RoundRobinPlanner
{
    partial class PlannerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxUrl = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownTeams = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPeriods = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownFields = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxParallelFields = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxDateStart = new System.Windows.Forms.TextBox();
            this.numericUpDownSpare = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownRound = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDownMatch = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonPlan = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.labelMatchesCount = new System.Windows.Forms.Label();
            this.labelRoundCount = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelDateStart = new System.Windows.Forms.Label();
            this.labelDateEnd = new System.Windows.Forms.Label();
            this.labelDuration = new System.Windows.Forms.Label();
            this.labelSeparator = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTeams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPeriods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMatch)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxUrl);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(459, 53);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "API";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxParallelFields);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.numericUpDownFields);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.numericUpDownPeriods);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.numericUpDownTeams);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(13, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(223, 131);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Round Robin";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDownSpare);
            this.groupBox3.Controls.Add(this.textBoxDateStart);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.numericUpDownRound);
            this.groupBox3.Controls.Add(this.numericUpDownMatch);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(252, 72);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(220, 131);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Duration";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Url:";
            // 
            // textBoxUrl
            // 
            this.textBoxUrl.Location = new System.Drawing.Point(50, 19);
            this.textBoxUrl.Name = "textBoxUrl";
            this.textBoxUrl.ReadOnly = true;
            this.textBoxUrl.Size = new System.Drawing.Size(389, 20);
            this.textBoxUrl.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Teams count:";
            // 
            // numericUpDownTeams
            // 
            this.numericUpDownTeams.Location = new System.Drawing.Point(113, 27);
            this.numericUpDownTeams.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownTeams.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownTeams.Name = "numericUpDownTeams";
            this.numericUpDownTeams.Size = new System.Drawing.Size(94, 20);
            this.numericUpDownTeams.TabIndex = 1;
            this.numericUpDownTeams.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numericUpDownPeriods
            // 
            this.numericUpDownPeriods.Location = new System.Drawing.Point(113, 53);
            this.numericUpDownPeriods.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownPeriods.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPeriods.Name = "numericUpDownPeriods";
            this.numericUpDownPeriods.Size = new System.Drawing.Size(94, 20);
            this.numericUpDownPeriods.TabIndex = 3;
            this.numericUpDownPeriods.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Periods count:";
            // 
            // numericUpDownFields
            // 
            this.numericUpDownFields.Location = new System.Drawing.Point(113, 79);
            this.numericUpDownFields.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownFields.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownFields.Name = "numericUpDownFields";
            this.numericUpDownFields.Size = new System.Drawing.Size(94, 20);
            this.numericUpDownFields.TabIndex = 5;
            this.numericUpDownFields.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Fields count:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Are fields parallel?";
            // 
            // checkBoxParallelFields
            // 
            this.checkBoxParallelFields.AutoSize = true;
            this.checkBoxParallelFields.Checked = true;
            this.checkBoxParallelFields.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxParallelFields.Location = new System.Drawing.Point(113, 105);
            this.checkBoxParallelFields.Name = "checkBoxParallelFields";
            this.checkBoxParallelFields.Size = new System.Drawing.Size(87, 17);
            this.checkBoxParallelFields.TabIndex = 7;
            this.checkBoxParallelFields.Text = "Parallel fields";
            this.checkBoxParallelFields.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Date start:";
            // 
            // textBoxDateStart
            // 
            this.textBoxDateStart.Location = new System.Drawing.Point(106, 19);
            this.textBoxDateStart.Name = "textBoxDateStart";
            this.textBoxDateStart.Size = new System.Drawing.Size(94, 20);
            this.textBoxDateStart.TabIndex = 14;
            this.textBoxDateStart.Text = "now";
            // 
            // numericUpDownSpare
            // 
            this.numericUpDownSpare.Location = new System.Drawing.Point(106, 97);
            this.numericUpDownSpare.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownSpare.Name = "numericUpDownSpare";
            this.numericUpDownSpare.Size = new System.Drawing.Size(94, 20);
            this.numericUpDownSpare.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Spare rounds:";
            // 
            // numericUpDownRound
            // 
            this.numericUpDownRound.Location = new System.Drawing.Point(106, 71);
            this.numericUpDownRound.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownRound.Name = "numericUpDownRound";
            this.numericUpDownRound.Size = new System.Drawing.Size(94, 20);
            this.numericUpDownRound.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Round (days):";
            // 
            // numericUpDownMatch
            // 
            this.numericUpDownMatch.Location = new System.Drawing.Point(106, 45);
            this.numericUpDownMatch.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownMatch.Name = "numericUpDownMatch";
            this.numericUpDownMatch.Size = new System.Drawing.Size(94, 20);
            this.numericUpDownMatch.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Match (minutes):";
            // 
            // buttonPlan
            // 
            this.buttonPlan.Location = new System.Drawing.Point(12, 215);
            this.buttonPlan.Name = "buttonPlan";
            this.buttonPlan.Size = new System.Drawing.Size(459, 30);
            this.buttonPlan.TabIndex = 3;
            this.buttonPlan.Text = "Calculate matches count, rounds count, schedule duration";
            this.buttonPlan.UseVisualStyleBackColor = true;
            this.buttonPlan.Click += new System.EventHandler(this.buttonPlan_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelSeparator);
            this.groupBox4.Controls.Add(this.labelDuration);
            this.groupBox4.Controls.Add(this.labelDateEnd);
            this.groupBox4.Controls.Add(this.labelDateStart);
            this.groupBox4.Controls.Add(this.labelRoundCount);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.labelMatchesCount);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(13, 259);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(462, 145);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Plan";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Matches count:";
            // 
            // labelMatchesCount
            // 
            this.labelMatchesCount.AutoSize = true;
            this.labelMatchesCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMatchesCount.Location = new System.Drawing.Point(113, 26);
            this.labelMatchesCount.Name = "labelMatchesCount";
            this.labelMatchesCount.Size = new System.Drawing.Size(14, 13);
            this.labelMatchesCount.TabIndex = 1;
            this.labelMatchesCount.Text = "?";
            // 
            // labelRoundCount
            // 
            this.labelRoundCount.AutoSize = true;
            this.labelRoundCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRoundCount.Location = new System.Drawing.Point(113, 49);
            this.labelRoundCount.Name = "labelRoundCount";
            this.labelRoundCount.Size = new System.Drawing.Size(14, 13);
            this.labelRoundCount.TabIndex = 3;
            this.labelRoundCount.Text = "?";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Rounds count:";
            // 
            // labelDateStart
            // 
            this.labelDateStart.AutoSize = true;
            this.labelDateStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDateStart.Location = new System.Drawing.Point(15, 120);
            this.labelDateStart.Name = "labelDateStart";
            this.labelDateStart.Size = new System.Drawing.Size(14, 13);
            this.labelDateStart.TabIndex = 4;
            this.labelDateStart.Text = "?";
            // 
            // labelDateEnd
            // 
            this.labelDateEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDateEnd.Location = new System.Drawing.Point(294, 120);
            this.labelDateEnd.Name = "labelDateEnd";
            this.labelDateEnd.Size = new System.Drawing.Size(150, 13);
            this.labelDateEnd.TabIndex = 5;
            this.labelDateEnd.Text = "?";
            this.labelDateEnd.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDuration
            // 
            this.labelDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDuration.Location = new System.Drawing.Point(128, 81);
            this.labelDuration.Name = "labelDuration";
            this.labelDuration.Size = new System.Drawing.Size(206, 14);
            this.labelDuration.TabIndex = 6;
            this.labelDuration.Text = "?";
            this.labelDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSeparator
            // 
            this.labelSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelSeparator.Location = new System.Drawing.Point(12, 105);
            this.labelSeparator.Name = "labelSeparator";
            this.labelSeparator.Size = new System.Drawing.Size(438, 5);
            this.labelSeparator.TabIndex = 7;
            // 
            // PlannerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 416);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.buttonPlan);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "PlannerForm";
            this.Text = "Planner Form (Sports Scheduler)";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTeams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPeriods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMatch)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxParallelFields;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDownFields;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDownPeriods;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownTeams;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownSpare;
        private System.Windows.Forms.TextBox textBoxDateStart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownRound;
        private System.Windows.Forms.NumericUpDown numericUpDownMatch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonPlan;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label labelRoundCount;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelMatchesCount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelSeparator;
        private System.Windows.Forms.Label labelDuration;
        private System.Windows.Forms.Label labelDateEnd;
        private System.Windows.Forms.Label labelDateStart;
    }
}

