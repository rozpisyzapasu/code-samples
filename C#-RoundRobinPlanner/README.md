# Plánovač rozpisů každý s každým v C# 

Součástí diplomky bylo vytvoření desktopového klienta, který se umí připojit na API a vytvořit plán pro rozpis každý s každým. Zdrojáky k projektu jsou dostupné na [Bitbucket](https://bitbucket.org/sports-scheduler/code-samples/src/tip/C%23-RoundRobinPlanner/RoundRobinPlanner/RoundRobinPlanner/).

## Princip komunikace s API

1.  Vytvoření HTTP požadavku - transformace struktury s daty JSON, nastavení hlaviček.
2.  Odeslání požadavku a přečtení těla zprávy z odpovědi, která obsahuje JSON s plánem.
3.  Převod JSON plánu do struktury, která je používána ve formulářové aplikaci.

## Implementace

.NET podporuje HTTP komunikaci přes třídy v [System.Net](http://stackoverflow.com/a/15091622). Pro práci s formátem JSON jsem použil knihovnu [Json.NET](http://james.newtonking.com/json). V Json.NET existuje několik možnosti transformací mezi objekty a JSON formátem. Já jsem využil přístup přes anonymní typy. Implementace komunikace s API najde v třídě [Planner](https://bitbucket.org/sports-scheduler/code-samples/src/tip/C%23-RoundRobinPlanner/RoundRobinPlanner/RoundRobinPlanner/Classes/Planner.cs). Zde vybírám několik ukázek, jak komunikovat s API:

### Princip komunikace s API v kódu

```csharp
string jsonRequest = requestToJson(request);
var http = createHttpRequest(jsonRequest);
var jsonResponse = readJsonResponse(http);
return jsonToPlan(jsonResponse);
```

### Vytvoření HTTP požadavku

```csharp
var http = (HttpWebRequest)WebRequest.Create(new Uri(apiUrl));
http.Accept = "application/json";
http.ContentType = "application/json";
http.Method = "POST";

ASCIIEncoding encoding = new ASCIIEncoding();
Byte[] bytes = encoding.GetBytes(jsonRequest);

Stream newStream = http.GetRequestStream();
newStream.Write(bytes, 0, bytes.Length);
newStream.Close();
```

### Přečtení JSON z těla HTTP response

```csharp
var response = http.GetResponse();
var stream = response.GetResponseStream();
var sr = new StreamReader(stream);
return sr.ReadToEnd();
```

### JSON převod přes anonymní typy

```csharp
String jsonResponse = "{...}";
var definition = new
{
    matchesCount = 0,
    roundsCount = 0,
    durationPlan = new
    {
        scheduleTime = "",
        dateStart = "",
        dateEnd = "",
    }
};
var objectResponse = JsonConvert.DeserializeAnonymousType(jsonResponse, definition);
```

## Screenshot z aplikace

![image](https://cloud.githubusercontent.com/assets/7994022/21351092/22d40eb8-c6bb-11e6-865a-22834e4112fe.png)
