
# Ukázkové integrace [API rozpisů zápasů](http://docs.rozpisyzapasu.apiary.io/)

## [Diplomová práce 2014](http://dspace.upce.cz/handle/10195/56555)

* [Tvorba JSON requestů v PHP](/PHP-Request-Builders)
* [Plánovač rozpisů každý s každým v C#](https://bitbucket.org/sports-scheduler/code-samples/src/default/C%23-RoundRobinPlanner/)

## Kontakt

Máte vlastní ukázky? Máte nápad na kód, který by se tady měl objevit?
Nebo máte problém s integrací [API](http://docs.rozpisyzapasu.apiary.io/)?

Napište mi na email **rozpisyzapasu@gmail.com**.