﻿<?php
require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/schedule-types-definition.php';
require_once __DIR__ . '/builders-factory.php';


$teams = array('Team 1', 'Team 2', 'Team 3', 'Team 4', 'Team 5', 'Team 6');
$availableRequests = array(
    ROUNDROBIN_KIRKMAN => 
        duration(
            roundRobin()->teamsArray($teams)->fields(2)->periods(2)->freeTeamReferees()
        )->startAt('now 10:50:45')->matchMinutes(32),
    ROUNDROBIN_BALANCED_FIELDS => 
        duration(
            roundRobin()->balancedFields()->teamsArray($teams)->fields(2)->periods(2)
        )->startAt('now 10:00')->matchMinutes(60)->roundDays(7)->spareRounds('now + 7 days'),
    ROUNDROBIN_BALANCED_FREETIME =>
        roundRobin()->balancedFreeTime()->teamsArray($teams)->matchesInRound(3)
        ->professionalReferees('Referee 2', 'Referee 2', 'Referee 3'),
    TOURNAMENT_GROUPS => groups()->groups(array(
        'A' => array('Team 1', 'Team 2', 'Team 3'),
        'B' => array('Team 4', 'Team 5', 'Team 6'),
        'C' => array('Team 7', 'Team 8', 'Team 9')
    ))->matchesInRound(2),
    TOURNAMENT_TWO_CONFERENCES => twoConferences()->groups(array(
        'A' => array('Team 1', 'Team 2', 'Team 3'),
        'B' => array('Team 4', 'Team 5', 'Team 6')
    ))->crossTable(),
    TABLE_CROSS => crossTable()->teamsArray($teams)->columns('C1', 'C2'),
    TABLE_CLASSIC => classicTable()->teamsArray($teams),
    PLAYOFF_SE => singleElimination()->teamsArray($teams)->bronzeMedalMatch(),
    PLAYOFF_DE => doubleElimination()->teamsArray($teams)->matchesInSerie(3),
    PLAYOFF_CONSOLATION => consolationPlayoff()->teamsArray($teams),
    STAGES_UNLIMITED => stages()
                            ->schedules(array(
                                'Season' => groups()->group('Team 1', 'Team 2')->group('Team 3', 'Team 4'),
                                'Winners' => singleElimination()->teams(
                                    teamReference('Season', 'A1'),
                                    teamReference('Season', 'B1')
                                )
                            ))
);


echo '<h1>PHP Request Builders</h1>';
foreach ($availableRequests as $type => $request) {
    $jsonRequest = json_encode($request->build(), JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    echo <<<SEC
    <h2>{$type}</h2>
    <pre>{$jsonRequest}</pre>
SEC;
}