<?php

namespace Tests\Build\Request;

abstract class RequestBuilder
{
    protected $teamsKey;
    protected $teams;

    public function teamsCount($teamsCount)
    {
        $this->teamsKey = 'teamsCount';
        $this->teams = $teamsCount;
        return $this;
    }

    public function teams()
    {
        $this->teamsKey = 'teams';
        $this->teams = func_get_args();
        return $this;
    }

    public function teamsArray(array $teams)
    {
        $this->teamsKey = 'teams';
        $this->teams = $teams;
        return $this;
    }

    abstract public function build();

    public function buildObject()
    {
        return json_decode(json_encode($this->build()));
    }
}
