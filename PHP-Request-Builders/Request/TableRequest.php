<?php

namespace Tests\Build\Request;

class TableRequest extends RequestBuilder
{
    private $tableType;
    private $columns;

    public function crossTable()
    {
        $this->tableType = TABLE_CROSS;
        return $this;
    }

    public function classicTable()
    {
        $this->tableType = TABLE_CLASSIC;
        return $this;
    }

    public function columns()
    {
        $this->columns = func_get_args();
        return $this;
    }

    public function build()
    {
        $request = array(
            'scheduleType' => $this->tableType,
            $this->teamsKey => $this->teams,
            'columns' => $this->columns
        );
        if (is_null($this->teamsKey)) {
            unset($request[$this->teamsKey]);
        }
        if (is_null($this->tableType)) {
            unset($request['scheduleType']);
        }
        if (is_null($this->columns)) {
            unset($request['columns']);
        }
        return $request;
    }
}
