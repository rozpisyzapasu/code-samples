<?php

namespace Tests\Build\Request\Helpers;

class FieldsInRequest
{
    private $fieldsCount;
    private $areFieldsParallel;

    public function setParallelFields($fieldsCount)
    {
        $this->fieldsCount = $fieldsCount;
        $this->areFieldsParallel = true;
    }

    public function setMatchesInRound($nonParallelMatchesInRound)
    {
        $this->fieldsCount = $nonParallelMatchesInRound;
        $this->areFieldsParallel = false;
    }

    public function decorate(array $request)
    {
        if (!is_null($this->fieldsCount)) {
            $request['fields'] = array(
                'count' => $this->fieldsCount,
                'areFieldsParallel' => $this->areFieldsParallel
            );
        }
        return $request;
    }
}
