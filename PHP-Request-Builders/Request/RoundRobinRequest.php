<?php

namespace Tests\Build\Request;

class RoundRobinRequest extends MatchesScheduleRequest
{
    private $type = ROUNDROBIN_KIRKMAN;

    public function balancedFields()
    {
        $this->type = ROUNDROBIN_BALANCED_FIELDS;
        return $this;
    }

    public function balancedFreeTime()
    {
        $this->type = ROUNDROBIN_BALANCED_FREETIME;
        return $this;
    }

    public function build()
    {
        $request = array(
            'scheduleType' => $this->type,
            $this->teamsKey => $this->teams,
            'periodsCount' => $this->periodsCount,
            'table' => $this->table
        );
        if (is_null($this->teamsKey)) {
            unset($request[$this->teamsKey]);
        }
        if (is_null($this->periodsCount)) {
            unset($request['periodsCount']);
        }
        if (is_null($this->table)) {
            unset($request['table']);
        }
        return parent::decorateMatchesRequest($request);
    }
}
