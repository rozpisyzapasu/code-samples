<?php

namespace Tests\Build\Request;

abstract class MatchesScheduleRequest extends RequestBuilder
{
    protected $periodsCount;
    protected $table;
    private $fields;
    private $refereeType;
    private $refereeNames;

    public function __construct()
    {
        $this->fields = new Helpers\FieldsInRequest();
    }

    public function fields($fieldsCount)
    {
        $this->fields->setParallelFields($fieldsCount);
        return $this;
    }

    public function matchesInRound($nonParallelMatchesInRound)
    {
        $this->fields->setMatchesInRound($nonParallelMatchesInRound);
        return $this;
    }

    public function periods($periodsCount)
    {
        $this->periodsCount = $periodsCount;
        return $this;
    }

    public function crossTable()
    {
        return $this->table(TABLE_CROSS, func_get_args());
    }

    public function classicTable()
    {
        return $this->table(TABLE_CLASSIC, func_get_args());
    }

    public function table($type, array $columns = array())
    {
        if (empty($columns)) {
            $this->table = array(
                'scheduleType' => $type
            );
        } else {
            $this->table = array(
                'scheduleType' => $type,
                'columns' => $columns
            );
        }
        return $this;
    }

    public function freeTeamReferees()
    {
        $this->refereeType = REFEREES_FREE;
        $this->refereeNames = func_get_args();
        return $this;
    }

    public function professionalReferees()
    {
        $this->refereeType = REFEREES_PROFI;
        $this->refereeNames = func_get_args();
        return $this;
    }

    protected function decorateMatchesRequest($request)
    {
        if ($this->refereeType) {
            $request['referees'] = array(
                'refereeType' => $this->refereeType,
                'refereeNames' => $this->refereeNames
            );
        }
        return $this->fields->decorate($request);
    }
}
