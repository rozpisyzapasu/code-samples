<?php

namespace Tests\Build\Request;

class PlayoffRequest extends RequestBuilder
{
    private $type;
    private $matchesInSerie;
    private $bronzeMedalMatch;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function matchesInSerie($matchesInSerieCount)
    {
        $this->matchesInSerie = $matchesInSerieCount;
        return $this;
    }

    public function bronzeMedalMatch()
    {
        $this->bronzeMedalMatch = true;
        return $this;
    }

    public function build()
    {
        $request = array(
            'scheduleType' => $this->type,
            $this->teamsKey => $this->teams,
            'matchesInSerieCount' => $this->matchesInSerie,
            'isBronzeMedalMatchPlayed' => $this->bronzeMedalMatch
        );
        if (is_null($this->teamsKey)) {
            unset($request[$this->teamsKey]);
        }
        if (is_null($this->matchesInSerie)) {
            unset($request['matchesInSerieCount']);
        }
        if (is_null($this->bronzeMedalMatch)) {
            unset($request['isBronzeMedalMatchPlayed']);
        }
        return $request;
    }
}
