<?php

namespace Tests\Build\Request;

class DurationDecorator extends RequestBuilder
{
    private $request;
    private $dateStart;
    private $matchDuration;
    private $roundDuration;
    private $spareRoundsKey;
    private $spareRounds;

    public function __construct(RequestBuilder $request)
    {
        $this->request = $request;
    }

    public function startAt($dateStart)
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    public function matchMinutes($matchDuration)
    {
        $this->matchDuration = $matchDuration;
        return $this;
    }

    public function roundDays($roundDuration)
    {
        $this->roundDuration = $roundDuration;
        return $this;
    }

    public function spareRoundsCount($spareRounds)
    {
        $this->spareRoundsKey = 'spareRoundsCount';
        $this->spareRounds = $spareRounds;
        return $this;
    }

    public function spareRounds()
    {
        $this->spareRoundsKey = 'spareRounds';
        $this->spareRounds = func_get_args();
        return $this;
    }

    public function build()
    {
        $request = array(
            'dateStart' => $this->dateStart,
            'match' => array(
                'durationInMinutes' => $this->matchDuration
            ),
            'round' => array(
                'durationInDays' => $this->roundDuration,
                $this->spareRoundsKey => $this->spareRounds
            )
        );
        if (is_null($this->dateStart)) {
            unset($request['dateStart']);
        }
        if (is_null($this->matchDuration)) {
            unset($request['match']);
        }
        if (is_null($this->roundDuration) && is_null($this->spareRounds)) {
            unset($request['round']);
        } elseif (is_null($this->roundDuration)) {
            unset($request['round']['durationInDays']);
        } elseif (is_null($this->spareRounds)) {
            unset($request['round'][$this->spareRoundsKey]);
        }
        return $this->decorate($request);

    }

    private function decorate($durationRequest)
    {
        $request = $this->request->build();
        $request['duration'] = $durationRequest;
        return $request;
    }
}
