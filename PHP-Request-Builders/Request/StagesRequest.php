<?php

namespace Tests\Build\Request;

class StagesRequest extends RequestBuilder
{
    private $schedules;

    public function schedules(array $schedules)
    {
        $this->schedules = $schedules;
        return $this;
    }

    public function build()
    {
        $request = array(
            'scheduleType' => STAGES_UNLIMITED,
            'stagesSchedules' => $this->getSchedules()
        );
        if (is_null($this->schedules)) {
            unset($request['stagesSchedules']);
        }
        return $request;
    }

    private function getSchedules()
    {
        $schedules = array();
        if (!is_null($this->schedules)) {
            foreach ($this->schedules as $id => $s) {
                if (is_array($s)) {
                    $schedules[$id] = $s;
                } else {
                    $schedules[$id] = $s->build();
                }
            }
        }
        return $schedules;
    }
}
