<?php

namespace Tests\Build\Request;

class GroupsRequest extends MatchesScheduleRequest
{
    private $groups;
    private $type;

    public function __construct($type)
    {
        $this->type = $type;
        parent::__construct();
    }

    public function groupsTeamsCounts()
    {
        foreach (func_get_args() as $teamsInGroup) {
            $this->groups[] = $teamsInGroup;
        }
        return $this;
    }

    public function groups(array $groups)
    {
        $this->groups = $groups;
        return $this;
    }

    public function group()
    {
        $this->groups[] = func_get_args();
        return $this;
    }

    public function build()
    {
        $request = array(
            'scheduleType' => $this->type,
            'periodsCount' => $this->periodsCount,
            'groups' => $this->groups,
            'table' => $this->table
        );
        if (is_null($this->groups)) {
            unset($request['groups']);
        }
        if (is_null($this->periodsCount)) {
            unset($request['periodsCount']);
        }
        if (is_null($this->table)) {
            unset($request['table']);
        }
        return parent::decorateMatchesRequest($request);
    }
}
