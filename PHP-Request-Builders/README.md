# PHP Request Builders

> Snadná tvorba JSON reprezentací rozpisu?

Pokud chcete naplánovat nebo vytvořit rozpis, tak je nutné vytvořit JSON request, který odešlete na server. Začne se to komplikovat, když začnete zjišťovat, jaké všechny parametry lze nastavovat třeba pro rozpis [každý s každým](http://docs.rozpisyzapasu.apiary.io/). A co potom když budete chtít tvořit tabulky, turnaje, časy zápasů, …? Zde si můžete přečíst moje zkušenosti z tvorby a testování aplikace.

## Jak to nedělat, aneb můj první postup

Nejprve jsem v každém testu definoval přímo daný požadavek, tj. v PHP jsem ho zapsal pomocí asociativního pole a následně převedl na objekt. Problémem se tak ale stala jakákoliv změna struktury požadavku, protože definice struktury se nacházela na několika místech.

Takový postup jsem aplikoval během první iterace. Asi den z druhé iterace jsem strávil přepisováním kódu a odstraňováním duplicit. Poučení bylo jasné: **vytvořit vrstvu mezi JSON reprezentací a nastavením rozpisu**. Ale jak?

## Test Data Builder

Nakonec jsem tvorbu požadavků implementoval pomocí Test Data Builderů, které zajistí čitelnou syntaxi a rozšiřitelnost a odstraní duplicity. Např. takto vypadá tvorba požadavku pro turnaj se 6 týmy každý s každým na 2 hřištích:

```php
$json = roundRobin()->teams('A', 'B', 'C', 'D', 'E', 'F')->fields(2)->build();
```

Nebudu tady popisovat všechny možné metody, místo toho se můžete podívat do kódu na [Bitbucket](https://bitbucket.org/sports-scheduler/code-samples/src/tip/PHP-Request-Builders/). Jsou tam připraveny [příklady](https://bitbucket.org/sports-scheduler/code-samples/src/tip/PHP-Request-Builders/index.php) pro každý typ rozpisu. Pro zajímavost se zobrazí i vygenerovaný JSON požadavek.

## Q&A

### Jak aktuální je ten projekt?

Jedná se o verzi, kterou používám v testech. Tzn. je vždy aktuální, protože jinak by mi nefungovala většina testů. Pokud provedu nějakou změnu, přidám nový typ rozpisu apod., tak se změna ihned objeví v [projektu](https://bitbucket.org/sports-scheduler/code-samples/src/tip/PHP-Request-Builders/).

### Neměla by tohle řešit spíše hypermedia místo mé aplikace?

Teoreticky ano. V pure-hypermedia API byste nejprve provedli požadavek třeba s rel `create-form` a do navráceného kódu byste zadali svá data. Já v tom vidím akorát zbytečný krok navíc, protože stejně musíte přes jaký atribut, objekt, … nastavit třeba počet hřišť. Doba samoučících klientů ještě nenastala.

### Proč build metoda vrátí asociativní pole a ne JSON?

Testovací framework i API převedou asociativní pole na JSON automaticky. Proto každá `build` metoda vrací pole. Pro testovací účely je tam metoda `buildObject`, která vrátí objekt reprezentující převedený JSON request, který jakoby přišel přes HTTP.