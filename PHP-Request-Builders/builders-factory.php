<?php

function roundRobin()
{
    return new Tests\Build\Request\RoundRobinRequest();
}

function singleElimination()
{
    return new Tests\Build\Request\PlayoffRequest(PLAYOFF_SE);
}

function doubleElimination()
{
    return new Tests\Build\Request\PlayoffRequest(PLAYOFF_DE);
}

function consolationPlayoff()
{
    return new Tests\Build\Request\PlayoffRequest(PLAYOFF_CONSOLATION);
}

function groups()
{
    return new Tests\Build\Request\GroupsRequest(TOURNAMENT_GROUPS);
}

function twoConferences()
{
    return new Tests\Build\Request\GroupsRequest(TOURNAMENT_TWO_CONFERENCES);
}

function stages()
{
    return new Tests\Build\Request\StagesRequest();
}

function teamReference($schedule, $team)
{
    return array(
        'scheduleName' => $schedule,
        'team' => $team
    );
}

function crossTable()
{
    $table = new Tests\Build\Request\TableRequest();
    return $table->crossTable();
}

function classicTable()
{
    $table = new Tests\Build\Request\TableRequest();
    return $table->classicTable();
}

function matchesAnalysis()
{
    return new \Tests\Build\Analysis\MatchesScheduleAnalysisBuilder();
}

function playoffAnalysis()
{
    return new \Tests\Build\Analysis\PlayoffAnalysisBuilder();
}

function duration(\Tests\Build\Request\RequestBuilder $request)
{
    return new \Tests\Build\Request\DurationDecorator($request);
}
