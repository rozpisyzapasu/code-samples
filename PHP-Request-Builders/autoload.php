<?php

spl_autoload_register('requestBuilderAutoload');

function requestBuilderAutoload($class)
{
    if (strpos($class, 'Tests\\Build\\Request\\') === 0) {
        $classPath = str_replace(
            array('Tests\\Build\\Request\\', '\\'),
            array('', DIRECTORY_SEPARATOR),
            $class
        );
        require_once __DIR__ . "/Request/{$classPath}.php";
    }
}