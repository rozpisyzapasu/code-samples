<?php
define('ROUNDROBIN_KIRKMAN', 'roundRobin/Kirkman');
define('ROUNDROBIN_BALANCED_FIELDS', 'roundRobin/BalancedFields');
define('ROUNDROBIN_BALANCED_FREETIME', 'roundRobin/BalancedFreeTime');
define('PLAYOFF_SE', 'playoff/SingleElimination');
define('PLAYOFF_DE', 'playoff/DoubleElimination');
define('PLAYOFF_CONSOLATION', 'playoff/Consolation');
define('TOURNAMENT_GROUPS', 'tournament/Groups');
define('TOURNAMENT_TWO_CONFERENCES', 'tournament/TwoConferences');
define('STAGES_UNLIMITED', 'stages/Unlimited');
define('TABLE_CROSS', 'table/Cross');
define('TABLE_CLASSIC', 'table/Classic');

define('REFEREES_FREE', 'referee/FreeTeams');
define('REFEREES_PROFI', 'referee/Professionals');
